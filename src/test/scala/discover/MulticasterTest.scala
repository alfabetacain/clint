import org.scalatest._
import org.scalatest.Assertions._
import alfabetacain.clint.discover._
import java.net.DatagramPacket
import java.nio.ByteBuffer
import java.net.InetAddress

class MulticasterTest extends FlatSpec with DiscovererBehaviors {

  def getMulticaster(groupAddress: InetAddress, listenPort: Int, sendToPort: Int): Discoverer =
    new Multicaster(groupAddress, listenPort, sendToPort)

  "A Multicaster" should behave like UDPMulticastWrapper(getMulticaster)
}

