package alfabetacain.clint.discover 

import org.scalatest._
import org.scalatest.Assertions._
import alfabetacain.clint.discover._
import java.net.DatagramPacket
import java.nio.ByteBuffer
import java.net.InetAddress

class DecoderTest extends FlatSpec {

  def port = 42000
  def requestPacket = {
    val bytes = Array[Byte](UDPDecoder.REQUEST_TYPE)
    new DatagramPacket(bytes, bytes.length)
  }
  def replyPacketWithPort = {
    val bytes = Array[Byte](UDPDecoder.REPLY_TYPE)
    val bb = ByteBuffer.allocate(4)
    bb.putInt(port)
    val combined = bytes ++ bb.array
    new DatagramPacket(combined, combined.length)
  }
  def replyPacketNoPort = {
    val bytes = Array[Byte](UDPDecoder.REPLY_TYPE)
    new DatagramPacket(bytes, bytes.length)
  }

  "UDP decoder" should "correctly decode a request byte string" in {
    val packet = requestPacket
    val result = UDPDecoder.decode(packet)
    assert(Some(Request) == result)
  }

  it should "correctly decode a reply byte string with port" in {
    val packet = replyPacketWithPort
    val myAddress = InetAddress.getByName("localhost")
    packet.setAddress(myAddress)
    val result = UDPDecoder.decode(packet)
    assert(Some(Reply(myAddress, port)) == result)
  }

  it should "fail to decode reply byte string without port" in {
    val packet = replyPacketNoPort
    val result = UDPDecoder.decode(packet)
    assert(None == result)
  }

  it should "fail to decode a packet with no data" in {
    val packet = new DatagramPacket(Array[Byte](), 0)
    val result = UDPDecoder.decode(packet)
    assert(None == result)
  }

}
