import org.scalatest._
import org.scalatest.Assertions._
import alfabetacain.clint.discover._
import java.net._

trait DiscovererBehaviors { this: FlatSpec => 
  def UDPMulticastWrapper(getDiscoverer: (InetAddress, Int, Int) => Discoverer) = {

    def withDiscoverer(group: InetAddress, listenPort: Int, sendToPort: Int)(testCode: Discoverer => Any) = {
      var dis: Option[Discoverer] = None
      try {
        dis = Some(getDiscoverer(group, listenPort, sendToPort))
        testCode(dis.get)
      } finally {
        dis match {
          case None => ()
          case Some(value) => value.shutdown()
        }
      }
    }

    def withDoubleDiscoverer(group1: InetAddress, listenPort1: Int, sendToPort1: Int, group2: InetAddress, listenPort2: Int, sendToPort2: Int)(testCode: (Discoverer, Discoverer) => Any) = {
      withDiscoverer(group1, listenPort1, sendToPort1) {
        dis => withDiscoverer(group2, listenPort2, sendToPort2)(testCode(dis,_))
      }
    }

    val defaultPort1 = 42000
    val defaultPort2 = 42001
    val defaultGroupAddress = InetAddress.getByName("235.42.42.42")
    val defaultTestPort = 45000

    it should "be able to multicast to itself" in withDiscoverer(defaultGroupAddress, defaultPort1, defaultPort1) {
      dis =>
        dis.setVisible(Some(defaultTestPort))
        val results = dis.discover()
        assert(results.exists(rep => rep.port == defaultTestPort))
    }

    it should "not respond when not listening" in withDiscoverer(defaultGroupAddress, defaultPort1, defaultPort1) {
    dis =>
      dis.setVisible(None)
      val res = dis.discover()
      assert(List() == res)
    }

    it should "respond with the port set via setVisible" in withDiscoverer(defaultGroupAddress, defaultPort1, defaultPort1) {
    dis =>
      dis.setVisible(Some(defaultTestPort))
      val res = dis.discover
      assert(res.exists(rep => rep.port == defaultTestPort))
    }

    it should "not be able to see another if neither are visible" in withDoubleDiscoverer(defaultGroupAddress, defaultPort1, defaultPort2, defaultGroupAddress, defaultPort2, defaultPort1) {
      (dis1, dis2) =>
        dis1.setVisible(None)
        dis2.setVisible(None)
        val res1 = dis1.discover()
        val res2 = dis2.discover()
        assert(List() == res1)
        assert(List() == res2)
    }

    it should "should be able to see another if both are visible" in withDoubleDiscoverer(defaultGroupAddress, defaultPort1, defaultPort2, defaultGroupAddress, defaultPort2, defaultPort1) {
      (dis1, dis2) =>
        val dis1Port = 1
        val dis2Port = 2
        dis1.setVisible(Some(dis1Port))
        dis2.setVisible(Some(dis2Port))
        val res1 = dis1.discover()
        val res2 = dis2.discover()
        assert(res1.length == dis1Port)
        assert(res1.head.port == dis2Port)
        assert(res2.length == 1)
        assert(res2.head.port == dis1Port)
    }

    it should "be able to see only one if two are present and only one is visible" in withDoubleDiscoverer(defaultGroupAddress, defaultPort1, defaultPort2, defaultGroupAddress, defaultPort2, defaultPort1) {
      (dis1, dis2) =>
        val dis1Port = 1
        dis1.setVisible(Some(dis1Port))
        dis2.setVisible(None)
        val res1 = dis1.discover()
        val res2 = dis2.discover()
        assert(List() == res1)
        assert(res2.length == 1)
        assert(res2.head.port == dis1Port)
    }
  }
}
