package alfabetacain.clint

import java.net.Socket
import java.net.ServerSocket
import java.net.InetAddress
import java.io._
import scala.concurrent.ExecutionContext.Implicits.global
import akka.agent.Agent
import scala.Console

object Options {
  val MULTICAST_ADDRESS: String = "234.42.42.42"
  val ENCODING: String = "UTF-8"
  val M_SOCKET_PORT: Int = 42000
}

object runner {
  def main(args: Array[String]): Unit = {
    val mainThread = new Thread(new Runnable {
      def run(): Unit = {
        val node = new Node()
        val printer: String => MText => Unit = 
          name => msg => msg match {
            case MText(value) => println("\t" + name + ">" + value)
          }
        val messageHandler = if (args.length > 0) printer(args(0)) else printer("John Doe")
        val fileHandler: MFile => Option[OutputStream] = file => None
        var conn: Connection = null
        var multisocket = discover.getDiscoverer(Options.MULTICAST_ADDRESS, Options.M_SOCKET_PORT) // Discoverer(Options.MULTICAST_ADDRESS, Options.M_SOCKET_PORT)
        println("Ready for input: ")
        print(">")
        Console.out.flush
        for (ln <- io.Source.stdin.getLines) {
          if (ln.startsWith(":")) { //is command
            val temp = ln.substring(1, ln.length).split(" ").toList
            val parts = temp.head.toLowerCase :: temp.tail
            parts match {
              case "discover" :: Nil => 
                if (multisocket != null) {
                  println("Discovering...")
                  println(multisocket.discover())
                }
              case "exit" :: Nil => 
                multisocket.shutdown()
                node.shutdown()
                if (conn != null) {
                  conn.shutdown()
                }
                return
              case "incoming" :: Nil =>
                node.getIncomingConnections.foreach(tuple => println("Incoming: " + tuple._1))
              case "visible" :: value :: Nil => 
                if (multisocket != null) {
                  if (value.toBoolean) //visible true
                    multisocket.setVisible(Some(node.localPort))
                  else 
                    multisocket.setVisible(None) 
                } else 
                  println("No multisocket set")
              case "accept" :: name :: Nil =>
                node.acceptIncomingConnection(name, printer("Me"), fileHandler) match {
                  case Some(c) => conn = c
                  case None => println("Failed to connect to " + name)
                }
              case "connect" :: host :: port :: Nil => 
                conn = node.connectTo(host, port.toInt, messageHandler, fileHandler)
              case "connect" :: name :: host :: port :: Nil => 
                conn = node.connectTo(name, host, port.toInt, printer(name), fileHandler)
              case "close" :: Nil if conn != null => 
                conn.shutdown()
              case "info" :: Nil => 
                println("My address is: " + node.localAddress)
                println("My port is " + node.localPort)
              case m => println("Unknown command: " + m)
            }
          }
          else { //is not command
            if (conn != null) {
              conn.send(ln)
            }
          }
          if (conn == null) {
            print(">")
            Console.out.flush
          }
        }
      }
    })
    mainThread.start
  }
}

