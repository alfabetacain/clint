package alfabetacain.clint.discover

import java.net.DatagramPacket
import java.net.MulticastSocket
import java.net.InetAddress
import java.net.InetSocketAddress
import java.net.NetworkInterface
import akka.agent.Agent
import java.nio.ByteBuffer
import scala.concurrent.ExecutionContext.Implicits.global

class Multicaster(val group: InetAddress, val myPort: Int, val senderPort: Int) extends Discoverer {
  private val msocket = new MulticastSocket(myPort)
  def joinGroups(enumer: java.util.Enumeration[NetworkInterface], total: Int, notWorking: Int): (Int, Int) = {
    if (enumer.hasMoreElements) {
      val network = enumer.nextElement
      try  {
        msocket.joinGroup(new InetSocketAddress(group, senderPort), network)
        println("Supports multicast: " + network)
        joinGroups(enumer, total + 1, notWorking)
      } catch {
        case excep: Throwable => 
          joinGroups(enumer, total + 1, notWorking + 1)
      }
    } else
      (total, notWorking)
  }
  val (networksTotal, noMulticastNetworksTotal) = joinGroups(NetworkInterface.getNetworkInterfaces, 0, 0)
  if (networksTotal == noMulticastNetworksTotal)
    throw new IllegalStateException("No networks supports multicast!!!")
  println("NICs which support multicast: " + (networksTotal - noMulticastNetworksTotal))
  private var listening = false
  private var visible: Option[Int] = None
  private val replyAgent = Agent(Set(): Set[Reply])

  def this(group: InetAddress, myPort: Int) {
    this(group, myPort, myPort)
  }

  def this(multiAddress: String, myPort: Int, receivingPort: Int) {
    this(InetAddress.getByName(multiAddress), myPort)
  }

  def this(multiAddress: String, myPort: Int) {
    this(InetAddress.getByName(multiAddress), myPort, myPort)
  }

  private val listener = new Thread(new Runnable {
    var buff = new Array[Byte](UDPDecoder.MINIMUM_BUFFER_SIZE)
    var packet = new DatagramPacket(buff,buff.length)
    def run(): Unit = {
      while(true) {
        try {
          msocket.receive(packet)
        } catch {
          case _: Throwable => return
        }
        if (Thread.currentThread().isInterrupted()) {
          return
        }
        val decoded = UDPDecoder.decode(packet)
        decoded match {
          case None => println("Failed to decode udp message from: " + packet.getAddress() + ":" + packet.getPort())
          case Some(value) =>
            value match {
              case Request if !visible.isEmpty =>
                val typeBytes = Array[Byte](UDPDecoder.REPLY_TYPE)
                val myPort = visible.get
                val bb = ByteBuffer.allocate(4)
                bb.putInt(myPort)
                val portBytes = bb.array()
                val joined = typeBytes ++ portBytes
                val replyPacket = new DatagramPacket(joined, joined.length, packet.getAddress(), packet.getPort)
                msocket.send(replyPacket)
              case reply: Reply if listening => 
                replyAgent send (_ + reply)
              case _ => 
                println("Received message without match: " + decoded + " and listening: " + listening)
            }
          }
      }
    }
  })
  listener.start

  def discover(): List[Reply] =  {
    val packet = new DatagramPacket(Array(UDPDecoder.REQUEST_TYPE), 1, group, senderPort)
    listening = true
    msocket.send(packet)
    msocket.send(packet)
    msocket.send(packet)
    Thread.sleep(2000)
    listening = false
    println("Not listening")
    val results = replyAgent.get
    replyAgent send (_ => Set())
    results.toList
  }

  def setVisible(visiblePort: Option[Int]): Unit = {
    visible = visiblePort
  }

  def shutdown() {
    listener.interrupt()
    msocket.leaveGroup(group)
    msocket.close()
  }
}

