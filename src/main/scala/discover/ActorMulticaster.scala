package alfabetacain.clint.discover
import akka.actor._
import java.net._
import akka.io._
import akka.io.Inet._
import java.nio._
import java.nio.channels._
import akka.util._
import java.net._
import scala.language.postfixOps

private final case class Inet4ProtocolFamiliy() extends DatagramChannelCreator {
  override def create() = 
    DatagramChannel.open(StandardProtocolFamily.INET)
}

private final case class MulticastGroup(address: InetAddress) extends SocketOptionV2 {
  override def afterBind(s: DatagramSocket) = {
    val interfaces: java.util.Enumeration[NetworkInterface] = NetworkInterface.getNetworkInterfaces()
    while (interfaces.hasMoreElements)
      s.getChannel.join(address, interfaces.nextElement)
  }
}

case object Disc
case object GetPeers
case object Stop
class ActorMulticaster(val groupAddress: InetAddress, val myPort: Int, val senderPort: Int) extends Actor {
  import context.system
  var visible: Option[Int] = None
  var peers: List[InetSocketAddress] = List.empty[InetSocketAddress]
  val opts: List[akka.io.Inet.SocketOption] = List(Inet4ProtocolFamiliy(), MulticastGroup(groupAddress))
  IO(Udp) ! Udp.Bind(self, new InetSocketAddress(myPort), opts)


  override def receive = {
    case Udp.Bound(local) =>
      context.become(joined(sender()))
  }

  def joined(send: ActorRef): Receive = {
    case Udp.Received(data, remote) => 
      UDPDecoder.decode(data.asByteBuffer, remote.getAddress) match {
        case None => ()
        case Some(Request) if !visible.isEmpty => 
          send ! Udp.Send(ByteString(UDPDecoder.REPLY_TYPE) ++ ByteString(visible.get), remote)
      }
    case Disc =>
      send ! Udp.Send(ByteString(UDPDecoder.REQUEST_TYPE), new InetSocketAddress(groupAddress, senderPort))
      peers = List.empty
      context.become(discovering(send))
    case Some(value: Int) => visible = Some(value)
    case None => visible = None
    case Stop => context.stop(self)
  }

  def discovering(send: ActorRef): Receive = {
    case Udp.Received(data, remote) => 
      UDPDecoder.decode(data.asByteBuffer, remote.getAddress) match {
        case None => ()
        case Some(Request) if !visible.isEmpty =>
          send ! Udp.Send(ByteString(UDPDecoder.REPLY_TYPE) ++ ByteString(visible.get), remote)
        case Some(Reply(address, port)) => 
          peers = (new InetSocketAddress(address, port)) :: peers

      }
    case GetPeers =>
      sender() ! peers.map(address => Reply(address.getAddress, address.getPort))
      context.become(joined(send))
    case Some(value: Int) => visible = Some(value)
    case None => visible = None
    case Stop => context.stop(self)
  }
}

class MulticasterContext(val groupAddress: InetAddress, val myPort: Int, val senderPort: Int) extends Discoverer {
  import akka.pattern.ask
  import scala.concurrent._
  import scala.concurrent.duration._
  import akka.util._
  val actor = ActorSystem("mySystem").actorOf(Props(new ActorMulticaster(groupAddress, myPort, senderPort)))

  override def discover(): List[Reply] = {
    actor ! Disc
    Thread.sleep(5000)
    implicit val timeout = Timeout(2 seconds)
    val peers = actor ? GetPeers
    Await.result(peers, 2000.milliseconds).asInstanceOf[List[Reply]]
  }
  override def setVisible(visiblePort: Option[Int]): Unit = actor ! visiblePort
  override def shutdown(): Unit = actor ! Stop
}
