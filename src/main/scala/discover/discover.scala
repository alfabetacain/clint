package alfabetacain.clint {
package object discover {
  import discover._
  import java.net._

  def getDiscoverer(): discover.Discoverer = {
    ???
  }
  def getDiscoverer(groupAddress: String, port: Int): discover.Discoverer = {
    new Multicaster(groupAddress, port)
  }
  def getDiscoverer(groupAddress: InetAddress, listenPort: Int, sendToPort: Int) =
    new Multicaster(groupAddress, listenPort, sendToPort)
}
}

// Common types
package alfabetacain.clint.discover {

  trait Discoverer {
    def discover(): List[Reply]
    def setVisible(visiblePort: Option[Int]): Unit
    def shutdown(): Unit
  }

  sealed trait UDPMessage
  case object Request extends UDPMessage
  case class Reply(address: java.net.InetAddress, port: Int) extends UDPMessage

  private[discover] object UDPDecoder {
    import java.net._
    import java.nio._
    val REPLY_TYPE: Byte = 0
    val REQUEST_TYPE: Byte = 1

    def decode(packet: DatagramPacket): Option[UDPMessage] = {
      if (packet.getLength < 1)
        return None
      val bb = ByteBuffer.wrap(packet.getData())
      decode(bb, packet.getAddress())
    }

    def decode(buffer: ByteBuffer, remote: InetAddress): Option[UDPMessage] = {
      val typ = buffer.get
      typ match {
        case REQUEST_TYPE => Some(Request)
        case REPLY_TYPE => 
          if (buffer.limit < 5)
            return None
          else
            Some(Reply(remote, buffer.getInt))
        case _ => None
      }
    }

    val MINIMUM_BUFFER_SIZE = 5
  }
}
