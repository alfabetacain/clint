package alfabetacain.clint 

import java.net.Socket
import java.net.ServerSocket
import java.net.InetAddress
import java.io._
import scala.concurrent.ExecutionContext.Implicits.global
import akka.agent.Agent
import scala.Console


sealed trait Message
case class MText(msg: String) extends Message
case class MFile(name: String, bytes: Long) extends Message
case class MAccept(name: String) extends Message
case class MDecline(filename: String) extends Message

class Connection(val socket: Socket, val messageAction: MText => Unit, val fileAction: MFile => Option[OutputStream] ) {
  private var fileRequest: Option[MFile] = None
  private val out = new ObjectOutputStream(socket.getOutputStream())
  private val listener = new Thread(new Runnable {
    def run(): Unit = {
      val in = new ObjectInputStream(socket.getInputStream())
      while (true) {
        val obj: Option[Message] = try {
          Some(in.readObject().asInstanceOf[Message])
        } catch {
          case _: Throwable => {
            println("Connection closed")
            None
          }
        }
        if (Thread.currentThread.isInterrupted) {
          return
        }
        if (obj.isEmpty) 
          return
        obj.get match {
          case text: MText => messageAction(text)
          case file: MFile => 
            fileAction(file) match {
              case None => //file not accepted
                out.writeObject(MDecline(file.name))
              case Some(fileout) =>
                out.writeObject(MAccept(file.name))
                var readSoFar = 0L
                val buffer = new Array[Byte](1024)
                while (readSoFar < file.bytes) {
                  val read = socket.getInputStream().read(buffer)
                  fileout.write(buffer, 0, read)
                  readSoFar += read
                }
                fileout.close()
            }
          case MAccept(name) if !fileRequest.isEmpty => 
            if (fileRequest.get.name == name) {
              var sendSoFar = 0L
              val buffer = new Array[Byte](1024)
              val fileIn = new FileInputStream(new File(name))
              while (sendSoFar < fileRequest.get.bytes) {
                val read = fileIn.read(buffer)
                socket.getOutputStream.write(buffer, 0, read)
                sendSoFar += read
              }
              fileIn.close()
            }
        }
      }
    }
  })
  listener.start

  def send(text: String): Unit = {
    out.writeObject(MText(text))
  }
  
  def transferFile(filename: String): Unit = {
    val file = new File(filename)
    if (file.exists) {
      val size = file.length
      fileRequest = Some(MFile(filename, size))
      out.writeObject(fileRequest.get)
    }
  }

  def shutdown(): Unit = {
    listener.interrupt()
    socket.shutdownOutput()
    socket.shutdownInput()
    socket.close()
  }
}

case class Info(name: String)
class Node {
  private val server = new ServerSocket(0) //will just take an empty port
  private val connAgent = Agent(List(): List[(String, Socket)])
  val localPort = server.getLocalPort()
  val localAddress = InetAddress.getLocalHost() //will work most of the time, local pc name is also returned

  private val listener = new Thread(new Runnable {
    def run(): Unit = {
      while (true) {
        val incoming = try {
          server.accept()
        } catch {
          case _: Throwable => return
        }
        println("Incoming tcp connection")
        if (Thread.currentThread().isInterrupted()) {
          return
        }
        val in = new ObjectInputStream(incoming.getInputStream())
        val config = in.readObject().asInstanceOf[Info]
        println("received this config: " + config)
        connAgent send ((config.name, incoming) :: _)
      }
    }
  })
  listener.start

  def connectTo(host: String, port: Int, messageAction: MText => Unit, fileAction: MFile => Option[OutputStream]): Connection = {
    connectTo("Unknown", host, port, messageAction, fileAction)
  }

  def connectTo(name: String, host: String, port: Int, messageAction: MText => Unit, fileAction: MFile => Option[OutputStream]): Connection = {
    val socket = new Socket(host, port)
    val out = new ObjectOutputStream(socket.getOutputStream())
    out.writeObject(Info(name))
    new Connection(socket, messageAction, fileAction)
  }

  def getIncomingConnections(): List[(String, Socket)] = {
    connAgent.get
  }

  def acceptIncomingConnection(connName: String, messageAction: MText => Unit, fileAction: MFile => Option[OutputStream]): Option[Connection] = {
    val conns = connAgent.get
    conns.find(_._1 == connName) match {
      case Some(found) =>
        connAgent send (_.filter(_._1 != connName))
        Some(new Connection(found._2, messageAction, fileAction))
      case None => None
    }
  }
  
  def shutdown(): Unit = {
    listener.interrupt()
    server.close()
  }
}

