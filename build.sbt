name := "Command Line Interface for Network Transfer (CLINT)"

version := "0.1"

scalaVersion := "2.11.7"

libraryDependencies += "com.typesafe.akka" %% "akka-agent" % "2.4.0"
libraryDependencies += "com.typesafe.akka" %% "akka-actor" % "2.4.0"
libraryDependencies += "org.scalatest" % "scalatest_2.11" % "2.2.4" % "test"

scalacOptions += "-deprecation"
scalacOptions += "-feature"
